<?php
session_start();
include_once ('../../../vendor/autoload.php');

use App\OrgName\OrgID\Mobile\Mobile;
use App\OrgName\OrgID\Message\Message;
use App\OrgName\OrgID\Utility\Utility;

$mobile = new Mobile();
$singleTitle= $mobile->setData($_GET)->show();

?>
<!DOCTYPE html>
    <html>

        <body>
            <div id="message">
                <?php
                if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
                    echo Message::message();
                }?>
            </div>
            <button onclick="goBack()">Go Back</button>
            <br>
            <br>
            <form action="update.php" method="POST">
                <fieldset>
                    <legend>Edit Mobile information:</legend>
                        <level>Title</level>
                        <input type="hidden" name="id" value="<?php echo $singleTitle->id?>">
                        <input type="text" name="title" placeholder="Enter your mobile title" 
                               value="<?php echo $singleTitle->title?>">
            <br>
            <br>
                        <input type="submit" value="Update">
            <br>
                </fieldset>
            </form>

        </body>
    </html>

<script>
    function goBack() {
        window.history.back();
    }
</script>


