<?php
namespace App\OrgName\OrgID\Mobile;
    use App\OrgName\OrgID\Message\Message;
    use App\OrgName\OrgID\Utility\Utility;
    use PDO;
class Mobile{
    public $id="";
    public $title="";
    public $conn;
    public $serverName="localhost";
    public $userName="root";
    public $password="";
    public $lastEnterFirst="";
    public $deleted_at= "";
    public $created_at="";

    //Setting up the database connection using php
    public function __construct()
    {
        try {

            # MySQL with PDO_MYSQL
            $this->conn = new PDO("mysql:host=localhost;dbname=atomicproject", $this->userName, $this->password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    //Assigning value to properties
    public function setData($data=array()){


        if(array_key_exists('title',$data)){
            $this->title=filter_var($data['title'], FILTER_SANITIZE_STRING);
        }

        if (array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if (array_key_exists('lef',$data)){
            $this->lastEnterFirst=$data['lef'];
        }

            return $this;


    }
    //Storing data to database
    public function store(){
        try {
            $this->created_at=date('Y-m-d');
            $query = "INSERT INTO `atomicproject`.`mobile` (`title`,`created_at`) VALUES (:title,:created_at)";
            $result = $this->conn->prepare($query);
            $result->execute(array(':title' => $this->title, ':created_at' =>$this->created_at));
            if ($result) {
                Message::message("Success!Data has been stored successfully.");
                if($_POST['saveAdd']=="saveAdd"){
                    Utility::redirect("create.php");
                }else{
                    Utility::redirect("index.php");
                }

            }else {
                Message::message("Alert!Data has not been stored successfully.");
                Utility::redirect("index.php");

            }
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    //To show all data to index page
    public function index(){
        try {
            $_allinfo = array();
            $query = "SELECT * FROM `atomicproject`.`mobile` ORDER BY `title` ASC";
            $result = $this->conn->query($query);
            $_allinfo = $result->fetchAll(PDO::FETCH_OBJ);

            return $_allinfo;
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
    //To fetch the single row
    public function show(){
        try {
            $query = "SELECT * FROM `atomicproject`.`mobile` WHERE `id`=:id";
            $result = $this->conn->prepare($query);
            $result->execute(array(':id' => $this->id));
            $row = $result->fetch(PDO::FETCH_OBJ);
            return $row;
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }


    //To update the data

    public function update()
    {
        try {
            $query = "UPDATE `atomicproject`.`mobile` SET `title` = :title WHERE `mobile`.`id` =:id";
            $result = $this->conn->prepare($query);
            $result->execute(array(':title' => $this->title, 'id' => $this->id));

            if ($result) {
                Message::message("Data has been updated  successfully.");
                Utility::redirect("index.php");
            } else {
                Message::message("Data has not been updated  successfully.");
                Utility::redirect("index.php");
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function delete(){
        try{
            $query="DELETE FROM `atomicproject`.`mobile` WHERE `mobile`.`id`=".$this->id;
            $result = $this->conn->prepare($query);
            $result->execute(array(':id' => $this->id));
            if ($result) {
                Message::message("Data has been deleted successfully.");
                Utility::redirect("index.php");
            } else {
                Message::message("Data has not been deleted successfully.");
                Utility::redirect("index.php");
            }
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    //To check the uniqness of data
    public function is_unique(){
        $query="SELECT count(*) FROM `atomicproject`.`mobile` WHERE `mobile`.`title`='".$this->title."'";
        $result = $this->conn->prepare($query);
        $result->execute(array(':id' => $this->title));
        $row= $result->fetchColumn();

        if($row>0){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    //Pagination
    public function count()
    {
        try {
            $query = "SELECT COUNT(*) AS totalItem FROM `atomicproject`.`mobile`";
            $result = $this->conn->prepare($query);
            $result->execute(array(':id' => $this->title));
            $row = $result->fetch(PDO::FETCH_ASSOC);
            return $row['totalItem'];
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
    }


    public function paginate($pageStartFrom=0,$Limit=5){
        try {
            if (!empty($this->lastEnterFirst)) {
                $whrClause = "DESC";
            } else {
                $whrClause = "ASC";
            }

            $_allinfo = array();
            $query = "SELECT * FROM `atomicproject`.`mobile` WHERE `deleted_at` IS  NULL
                      ORDER BY `id` " . $whrClause . " LIMIT " . $pageStartFrom . "," . $Limit;
            $result = $this->conn->query($query);
            $_allinfo = $result->fetchAll(PDO::FETCH_OBJ);

            return $_allinfo;

        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    //Soft Delete
    public function trash()
    {
    try {
        $this->deleted_at = date('Y-m-d');
        $query = "UPDATE `atomicproject`.`mobile` SET `deleted_at` = :deleted_at
                  WHERE `mobile`.`id` = :id";

        $result = $this->conn->prepare($query);
        $result->execute(array(':deleted_at' => $this->deleted_at, ':id' => $this->id));

        if ($result) {
            Message::message("Mobile information has been trashed successfully.");
        } else {
            Message::message(" Cannot trash.");
        }

        Utility::redirect('index.php');
       }
    catch (PDOException $e) {
        echo $e->getMessage();
    }
    }

    public function trashed()
    {
        try {
            $_allinfo = array();
            $query = "SELECT * FROM `atomicproject`.`mobile` WHERE deleted_at IS NOT NULL";

            $result = $this->conn->query($query);
            $_allinfo = $result->fetchAll(PDO::FETCH_OBJ);


            return $_allinfo;
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    
    public function recover(){
        try{
            $query = "UPDATE `atomicproject`.`mobile` SET `deleted_at` = NULL WHERE `mobile`.`id` =" . $this->id;
            $result = $this->conn->prepare($query);
            $result->execute(array(':id' => $this->id));

            if ($result) {
                Message::message("Mobile information has been rocovered successfully.");
            } else {
                Message::message(" Cannot recovered.");
            }

        Utility::redirect('index.php');
        
        }catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
    public function deleteOlderData(){
        try{


            $stmt = "DELETE FROM `atomicproject`.`mobile` WHERE deleted_at<DATE_SUB(curdate(), INTERVAL 10 DAY)";
            $result = $this->conn->query($stmt);

            if ($result) {
                Message::message("10 days old data has been successfully deleted.");
            } else {
                Message::message(" Cannot deleted.");
            }

            Utility::redirect('trashed.php');

        }catch (PDOException $e) {
            echo $e->getMessage();
        }
    }














}